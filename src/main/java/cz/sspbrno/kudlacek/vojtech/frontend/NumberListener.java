package cz.sspbrno.kudlacek.vojtech.frontend;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

public class NumberListener implements ChangeListener<String> {
    private TextField textField;

    NumberListener(TextField textField) {
        this.textField = textField;
    }

    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        if (!newValue.matches("\\d*")) {
            this.textField.setText(newValue.replaceAll("[^\\d]", ""));
        }

        if (this.textField.getText().length() > 1 || this.textField.getText().equals("9")) {
            this.textField.setText(this.textField.getText().substring(0, 1));
            this.textField.setText(this.textField.getText().replace("9", ""));
        }
    }
}
