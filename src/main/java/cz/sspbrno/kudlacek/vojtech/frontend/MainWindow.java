package cz.sspbrno.kudlacek.vojtech.frontend;

import cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm.*;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class MainWindow extends Application implements Initializable {

    public TextField input00;
    public TextField input10;
    public TextField input01;
    public TextField input20;
    public TextField input11;
    public TextField input21;
    public TextField input02;
    public TextField input12;
    public TextField input22;

    public TextField output00;
    public TextField output10;
    public TextField output01;
    public TextField output20;
    public TextField output11;
    public TextField output21;
    public TextField output02;
    public TextField output12;
    public TextField output22;

    public TextField[][] inputs = new TextField[3][3];
    public TextField[][] outputs = new TextField[3][3];

    public ListView resultListView;
    public Label countOfSteps;
    public Label timeToSolve;

    private GridTree gridTree;

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/cz/sspbrno/kudlacek/vojtech/frontend/MainWindow.fxml"));
        GridPane root = loader.load();
        Scene scene = new Scene(root);

        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.setTitle("Puzzle 8 (AI) Solver");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        inputs[0][0] = input00;
        inputs[1][0] = input10;
        inputs[2][0] = input20;
        inputs[0][1] = input01;
        inputs[1][1] = input11;
        inputs[2][1] = input21;
        inputs[0][2] = input02;
        inputs[1][2] = input12;
        inputs[2][2] = input22;

        outputs[0][0] = output00;
        outputs[1][0] = output10;
        outputs[2][0] = output20;
        outputs[0][1] = output01;
        outputs[1][1] = output11;
        outputs[2][1] = output21;
        outputs[0][2] = output02;
        outputs[1][2] = output12;
        outputs[2][2] = output22;

        for (TextField[] inp : inputs) {
            for (TextField input : inp) {
                input.textProperty().addListener(new NumberListener(input));
            }
        }

        for (TextField[] out : outputs) {
            for (TextField output : out) {
                output.textProperty().addListener(new NumberListener(output));
            }
        }
    }

    public void onClickSolve(ActionEvent actionEvent) {
        long startTime = System.nanoTime();
        Cell[][] startCells = new Cell[3][3];
        Cell[][] endCells = new Cell[3][3];

        for (int i = 0; i < inputs.length; i++) {
            for (int j = 0; j < inputs[i].length; j++) {
                startCells[i][j] = new Cell(Byte.parseByte(inputs[i][j].getText()));
            }
        }

        for (int i = 0; i < outputs.length; i++) {
            for (int j = 0; j < outputs[i].length; j++) {
                endCells[i][j] = new Cell(Byte.parseByte(outputs[i][j].getText()));
            }
        }

        gridTree = new GridTreeBruteForce(new Grid(startCells), new Grid(endCells));
        Grid result = gridTree.findResult();
        long time = (long) ((System.nanoTime() - startTime) / Math.pow(10, 6));
        timeToSolve.setText(time + "ms");
        ArrayList<Grid> grids = new ArrayList<>();
        grids.add(result);
        while (grids.get(grids.size() - 1).getParentGrid() != null) {
            grids.add(grids.get(grids.size() - 1).getParentGrid());
        }
        countOfSteps.setText(String.valueOf(grids.size()));

        resultListView.setEditable(true);
        ArrayList<String> numbers = new ArrayList<>();
        for (int i = 0; i < grids.size(); i++) {
            numbers.add(i + ". ");
        }
        ObservableList<String> steps = FXCollections.observableList((List) numbers);
        resultListView.setItems(steps);
        resultListView.setOnMouseClicked(event -> {
            int indexForArray = (grids.size() - resultListView.getSelectionModel().getSelectedIndex()) - 1;

            for (int i = 0; i < outputs.length; i++) {
                for (int j = 0; j < outputs[i].length; j++) {
                    outputs[i][j].setText(grids.get(indexForArray).getCells()[i][j].getValue() + "");
                }
            }
        });
    }

    public void onClickAI(ActionEvent actionEvent) {
    }

    public void onClickSolveHeuristics(ActionEvent actionEvent) {
        long startTime = System.nanoTime();
        Cell[][] startCells = new Cell[3][3];
        Cell[][] endCells = new Cell[3][3];

        for (int i = 0; i < inputs.length; i++) {
            for (int j = 0; j < inputs[i].length; j++) {
                startCells[i][j] = new Cell(Byte.parseByte(inputs[i][j].getText()));
            }
        }

        for (int i = 0; i < outputs.length; i++) {
            for (int j = 0; j < outputs[i].length; j++) {
                endCells[i][j] = new Cell(Byte.parseByte(outputs[i][j].getText()));
            }
        }

        gridTree = new GridTreeHeuristic(new Grid(startCells), new Grid(endCells));
        Grid result = gridTree.findResult();
        long time = (long) ((System.nanoTime() - startTime) / Math.pow(10, 6));
        timeToSolve.setText(time + "ms");
        ArrayList<Grid> grids = new ArrayList<>();
        grids.add(result);
        while (grids.get(grids.size() - 1).getParentGrid() != null) {
            grids.add(grids.get(grids.size() - 1).getParentGrid());
        }
        countOfSteps.setText(String.valueOf(grids.size()));

        resultListView.setEditable(true);
        ArrayList<String> numbers = new ArrayList<>();
        for (int i = 0; i < grids.size(); i++) {
            numbers.add(i + ". ");
        }
        ObservableList<String> steps = FXCollections.observableList((List) numbers);
        resultListView.setItems(steps);
        resultListView.setOnMouseClicked(event -> {
            int indexForArray = (grids.size() - resultListView.getSelectionModel().getSelectedIndex()) - 1;

            for (int i = 0; i < outputs.length; i++) {
                for (int j = 0; j < outputs[i].length; j++) {
                    outputs[i][j].setText(grids.get(indexForArray).getCells()[i][j].getValue() + "");
                }
            }
        });
    }
}
