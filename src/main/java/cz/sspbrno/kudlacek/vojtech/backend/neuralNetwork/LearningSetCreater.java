package cz.sspbrno.kudlacek.vojtech.backend.neuralNetwork;

import cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm.Grid;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class LearningSetCreater {

    public static void main(String[] args) throws IOException, InterruptedException {
        Grid[] inputGrids = new Grid[100000];
        ArrayList<Grid>[] results = new ArrayList[100000];
        for (int i = 0; i < 100000; i++) {
            new Thread(new LearnigSetCreatorThread(i, inputGrids, results)).start();
        }
        Thread.sleep(2400000L);

        BufferedWriter bwIn = new BufferedWriter(new FileWriter(new File("learningSetIn")));
        BufferedWriter bwOut = new BufferedWriter( new FileWriter(new File("learningSetOut")));
        for (int i = 0; i < 100000; i++) {
            InputParser inputParser = new InputParser(inputGrids[i], inputGrids[i]);
            bwIn.write(Arrays.toString(inputParser.parseInputs()));
            bwIn.newLine();
            if (results[i] == null){
                bwOut.write(" ");
                bwOut.newLine();
                continue;
            }
            OutputParser outputParser = new OutputParser(results[i]);
            bwOut.write(Arrays.toString(outputParser.parse()));
            bwOut.newLine();
        }
        bwIn.flush();
        bwIn.close();
        bwOut.flush();
        bwOut.close();
    }
}
