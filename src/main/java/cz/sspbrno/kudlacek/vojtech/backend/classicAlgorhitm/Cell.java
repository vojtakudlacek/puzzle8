package cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm;

public class Cell {
    private byte value;

    public Cell(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return value;
    }

    public void setValue(byte value) {
        this.value = value;
    }

    @Override
    protected Object clone() {
        return new Cell(value);
    }

    @Override
    public boolean equals(Object obj) {
        return this.value == ((Cell) obj).value;
    }

    @Override
    public String toString() {
        return "Cell{" +
                "value=" + value +
                '}';
    }
}
