package cz.sspbrno.kudlacek.vojtech.backend.neuralNetwork;

import cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm.Cell;
import cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm.Grid;
import org.apache.commons.lang3.ArrayUtils;

public class InputParser {

    private Grid initState;
    private Grid outState;
    public InputParser(Grid initState, Grid outState) {
        this.initState = initState;
        this.outState = outState;
    }

    public double[] parseInputs(){
        double[] init = parse(initState);
        double[] out = parse(outState);
        double[] result = ArrayUtils.addAll(init, out);
        return result;
    }

    private double[] parse(Grid state) {
        Cell[][] cells = state.getCells();
        double[] result = new double[9];
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                result[i*3+j] = cells[j][j].getValue();
            }
        }
        return result;
    }

}
