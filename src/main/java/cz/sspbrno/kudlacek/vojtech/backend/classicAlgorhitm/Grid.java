package cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm;

import java.util.Arrays;

public class Grid {
    public Grid getParentGrid() {
        return parentGrid;
    }

    private Grid parentGrid;
    private Cell[][] cells;

    public Grid(Cell[][] grid) {
        this.cells = grid;
    }

    public boolean moveCell(byte fromX, byte fromY, byte vectX, byte vectY) {
        if ((fromX > 2 || fromY > 2) || (vectX != 0 && vectY != 0))
            return false;
        Cell tmp = cells[fromX][fromY];
        try {
            Cell tmp2 = cells[fromX + vectX][fromY + vectY];
            cells[fromX][fromY] = tmp2;
            cells[fromX + vectX][fromY + vectY] = tmp;
        } catch (ArrayIndexOutOfBoundsException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = true;
        Cell[][] newCells = ((Grid) obj).cells;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                result = result && this.cells[i][j].equals(newCells[i][j]);
            }
        }
        return result;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public Grid clone() {
        Cell[][] newCells = new Cell[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                newCells[i][j] = cells[i][j];
            }
        }
        return new Grid(newCells);
    }

    public void setParent(Grid parent) {
        this.parentGrid = parent;
    }

    public boolean isParent(Grid testParent) {
        return this.parentGrid.equals(testParent);
    }

    public boolean moveCell(byte vectX, byte vectY) {
        byte[] pos = findEmptyCell();
        return moveCell(pos[0], pos[1], vectX, vectY);
    }

    private byte[] findEmptyCell() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (this.cells[i][j] == null || this.cells[i][j].getValue() == (byte) 0)
                    return new byte[]{(byte) i, (byte) j};
            }
        }
        return null;
    }

    @Override
    public String toString() {


        return "Grid{" +
                "cells=[" + Arrays.toString(this.cells[0]) + ", " + Arrays.toString(this.cells[1]) +
                ", " + Arrays.toString(this.cells[2]) + " ]}";
    }
}
