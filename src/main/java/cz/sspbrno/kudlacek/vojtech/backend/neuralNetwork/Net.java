package cz.sspbrno.kudlacek.vojtech.backend.neuralNetwork;

import basicneuralnetwork.NeuralNetwork;
import basicneuralnetwork.activationfunctions.SigmoidActivationFunction;
import cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class Net {

    public static void main(String[] args) throws IOException {
        NeuralNetwork nn0;
        nn0 = new NeuralNetwork(18, 5, 400, 450); //TODO: poštelovat
        //nn0.setActivationFunction(new nullActivationFunction());
        double[][] inputs = loadLearingSet(true);
        double[][] outputs = loadLearingSet(false);
        int skips = 0;
        nn0.setLearningRate(0.0105); //TODO: Poštelovat

        //for (int j = 0; j < 20; j++) {

            for (int i = 0; i < inputs.length; i++) {
                double[] input = inputs[i];
                double[] output = outputs[i];
                if (output == null) {
                    continue;
                }
                nn0.train(input, output);
                //System.out.println(Arrays.toString(output));
                skips++;
            }
       // }

        //test
        System.out.println(skips);
        double[] in = new double[]{1d/10d+0.1, 2d/10d+0.1, 3d/10d+0.1, 4d/10d+0.1, 5d/10d+0.1, 6d/10d+0.1, 7d/10d+0.1, 8d/10d+0.1, 0d/10d+0.1, 1d/10d+0.1, 2d/10d+0.1, 3d/10d+0.1,4d/10d+0.1, 5d/10d+0.1, 6d/10d+0.1, 7d/10d+0.1, 0d/10d+0.1, 8d/10d+0.1}; //testing data
        double[] nn = nn0.guess(in);
        System.out.println(nn.length + ": " + Arrays.toString(nn));
    }

    private static double[][] loadLearingSet(boolean input) throws IOException {
        File selectedFile;
        if (input)
            selectedFile = new File("learningSetIn");
        else
            selectedFile = new File("learningSetOut");
        BufferedReader br = new BufferedReader(new FileReader(selectedFile));
        ArrayList<double[]> result = new ArrayList<>();
        String line;
        while ((line = br.readLine()) != null) {
            double[] d = utils.getDoubleArrayFromString(line);
            result.add(d);
        }

        double[][] realResult = new double[result.size()][input ? 18 : 450];
        for (int i = 0; i < result.size(); i++) {
            if (result.get(i) == null) {
                realResult[i] = null;
                continue;
            }
            for (int j = 0; j < result.get(i).length; j++) {
                realResult[i][j] = result.get(i)[j] / 10D + 0.1;

            }
            //realResult[i] = result.get(i);
        }
        return realResult;
    }
}
