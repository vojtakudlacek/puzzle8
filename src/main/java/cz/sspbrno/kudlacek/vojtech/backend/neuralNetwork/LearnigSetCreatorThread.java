package cz.sspbrno.kudlacek.vojtech.backend.neuralNetwork;

import cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm.Cell;
import cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm.Grid;
import cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm.GridTreeBruteForce;

import java.util.ArrayList;
import java.util.Random;

public class LearnigSetCreatorThread implements Runnable {
    int i;
    Grid[] inputGrids;
    ArrayList<Grid>[] results;
    public LearnigSetCreatorThread(int i, Grid[] inputGrids, ArrayList<Grid>[] results){
        this.i =i;
        this.inputGrids = inputGrids;
        this.results = results;
        //System.out.println("Thread " + i +": Created");
    }
    @Override
    public void run() {

        System.out.println("Thread " + i +": Started");
        Grid[] outputGrids = new Grid[100000];
        GridTreeBruteForce[] gridTreeBruteForces = new GridTreeBruteForce[100000];
        Random rnd = new Random();

        Cell[][] inputCells = new Cell[3][3];
        Cell[][] outputCells = new Cell[3][3];
        StringBuilder inputBan = new StringBuilder();
        StringBuilder outputBan = new StringBuilder();
        for (int j = 0; j < 9; j++) {
            int rndNumber = rnd.nextInt(9);
            while (inputBan.toString().contains(String.valueOf(rndNumber))){
                rndNumber = rnd.nextInt(9);
            }
            inputBan.append(rndNumber);
            inputCells[j/3][j%3] = new Cell((byte) rndNumber);

            rndNumber = rnd.nextInt(9);
            while (outputBan.toString().contains(String.valueOf(rndNumber))){
                rndNumber = rnd.nextInt(9);
            }
            outputBan.append(rndNumber);
            outputCells[j/3][j%3] = new Cell((byte) rndNumber);
        }
        inputGrids[i] = new Grid(inputCells);
        outputGrids[i] = new Grid(outputCells);
        gridTreeBruteForces[i] = new GridTreeBruteForce(inputGrids[i], outputGrids[i]);
        Grid result = gridTreeBruteForces[i].findResult();
        ArrayList<Grid> grids = new ArrayList<>();
        if (result == null)
            return;
        grids.add(result);
        while (grids.get(grids.size() - 1).getParentGrid() != null) {
            grids.add(grids.get(grids.size() - 1).getParentGrid());
        }
        results[i] = grids;
        System.out.println(gridTreeBruteForces[i]);

    }
}
