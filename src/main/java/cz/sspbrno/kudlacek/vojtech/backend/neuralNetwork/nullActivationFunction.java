package cz.sspbrno.kudlacek.vojtech.backend.neuralNetwork;

import basicneuralnetwork.activationfunctions.ActivationFunction;
import org.ejml.simple.SimpleMatrix;

public class nullActivationFunction implements ActivationFunction {
    @Override
    public SimpleMatrix applyActivationFunctionToMatrix(SimpleMatrix simpleMatrix) {
        return simpleMatrix.copy();
    }

    @Override
    public SimpleMatrix applyDerivativeOfActivationFunctionToMatrix(SimpleMatrix simpleMatrix) {
        return simpleMatrix.copy();
    }

    @Override
    public String getName() {
        return "MOJE";
    }
}
