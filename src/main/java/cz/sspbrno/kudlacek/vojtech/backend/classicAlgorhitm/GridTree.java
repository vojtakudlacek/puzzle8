package cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm;

import java.util.ArrayList;

public abstract class GridTree {
    protected Grid initState;
    protected Grid finalState;
    protected ArrayList<ArrayList<Grid>> data;

    public GridTree(Grid firstElement, Grid finalElement) {
        initState = firstElement;
        finalState = finalElement;
    }

    public Grid findResult(){
        long startTime = System.nanoTime();
        while (!utils.deepContains(this.data, finalState)) {
            this.createNewLayer();
            if (System.nanoTime() - startTime >=  600000000000L){
                System.out.println("timeout");
                break;
            }
        }
        if (System.nanoTime() - startTime >=  600000000000L){
            return null;
        }

        int[] indexes = utils.deepIndexOf(this.data, finalState);
        return this.data.get(indexes[1]).get(indexes[0]);
    }
    protected abstract void createNewLayer();
    protected abstract void addAllNewCombinations(ArrayList<Grid> layer, Grid initState);

    @Override
    public String toString() {
        return "GridTree{" +
                "initState=" + initState +
                ", finalState=" + finalState +
                '}';
    }
}
