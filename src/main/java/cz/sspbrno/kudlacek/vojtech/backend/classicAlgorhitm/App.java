package cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Cell[][] startCells = new Cell[3][3];
        Cell[][] endCells = new Cell[3][3];
        Scanner scn = new Scanner(System.in);
        GridTreeBruteForce gridTreeBruteForce = new GridTreeBruteForce(input(startCells, scn), input(endCells, scn));
        System.out.println(gridTreeBruteForce.findResult());
    }

    private static Grid input(Cell[][] cells, Scanner scn) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print("X - " + i + " Y - " + j + ": ");
                cells[i][j] = new Cell(scn.nextByte());
                scn.nextLine();
            }
        }

        return new Grid(cells);
    }
}
