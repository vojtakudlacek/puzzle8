package cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm;

import java.util.ArrayList;

public class GridTreeBruteForce extends GridTree {

    public GridTreeBruteForce(Grid firstElement, Grid finalElement) {
        super(firstElement, finalElement);
        this.data = new ArrayList<>();
    }

    protected void createNewLayer() {
        int pos = data.size();
        ArrayList<Grid> layer = new ArrayList<>();
        if (!(pos == 0)) {
            for (Grid d : this.data.get(pos - 1)) {
                addAllNewCombinations(layer, d);
            }
        } else {
            addAllNewCombinations(layer, initState);
        }
        this.data.add(layer);
    }

    protected void addAllNewCombinations(ArrayList<Grid> layer, Grid initState) {
        Grid newGrid = initState.clone();
        newGrid.setParent(initState);
        if (newGrid.moveCell((byte) 0, (byte) 1))
            if (!utils.deepContains(this.data, newGrid))
                layer.add(newGrid);
        newGrid = initState.clone();
        newGrid.setParent(initState);
        if (newGrid.moveCell((byte) 0, (byte) -1))
            if (!utils.deepContains(this.data, newGrid))
                layer.add(newGrid);
        newGrid = initState.clone();
        newGrid.setParent(initState);

        if (newGrid.moveCell((byte) 1, (byte) 0))
            if (!utils.deepContains(this.data, newGrid))
                layer.add(newGrid);
        newGrid = initState.clone();
        newGrid.setParent(initState);
        if (newGrid.moveCell((byte) -1, (byte) 0))
            if (!utils.deepContains(this.data, newGrid))
                layer.add(newGrid);
    }

    @Override
    public String toString() {
        return "GridTree{" +
                "initState=" + initState +
                ", finalState=" + finalState +
                '}';
    }
}
