package cz.sspbrno.kudlacek.vojtech.backend.neuralNetwork;

import cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm.Cell;
import cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm.Grid;

import java.util.ArrayList;
import java.util.Arrays;

public class OutputParser {

    private ArrayList<Grid> result;

    public OutputParser(ArrayList<Grid> result) {
        this.result = result;
    }

    public double[] parse() {
        double[] data = new double[450];
        Arrays.fill(data, 0);

        for (int i1 = 0; i1 < result.size(); i1++) {
            Grid g = result.get(i1);
            Cell[][] cells = g.getCells();
            for (int i = 0; i < cells.length; i++) {
                for (int j = 0; j < cells[i].length; j++) {
                    data[i * cells[i].length + j + i1 * 9] = cells[i][j].getValue();
                }
            }
        }
        return data;
    }

    public ArrayList<Grid> parseBack(double[] data){
        ArrayList<Grid> arrayList = new ArrayList<>();
        for (int i = 0; i < data.length; i++) {
        }
        return arrayList;
    }
}
