package cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm;

import java.util.ArrayList;

public class utils {

    public static boolean deepContains(ArrayList inputArray, Object dataToCompare) {
        ArrayList<ArrayList> datas = inputArray;
        for (ArrayList<Grid> data : datas) {
            for (Grid d : data) {
                if (d.equals((Grid) dataToCompare))
                    return true;
            }
        }
        return false;
    }

    public static int[] deepIndexOf(ArrayList inputArray, Object dataToFind) {
        ArrayList<ArrayList> datas = inputArray;
        int result[] = new int[2];
        for (int i = 0; i < datas.size(); i++) {
            result[0] = datas.get(i).indexOf(dataToFind);
            if (result[0] != -1) {
                result[1] = i;
                break;
            }
        }
        return result[0] == -1 ? null : result;
    }

    public static double[] getDoubleArrayFromString(String data){
        if (data.equals(" ") || data.equals("") || data.equals("\n"))
            return null;
        data = data.replaceAll("\\[","").replaceAll("]","").replaceAll(" ","");
        String[] datas = data.split(",");
        double[] result = new double[datas.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = Double.parseDouble(datas[i]);
        }
        return result;
    }
}
