package cz.sspbrno.kudlacek.vojtech.backend.classicAlgorhitm;

import java.util.ArrayList;

public class GridTreeHeuristic extends GridTree {
    private ArrayList<ArrayList<Integer>> weights;
    private ArrayList<Integer> biggestWeights;

    public GridTreeHeuristic(Grid firstElement, Grid finalElement) {
        super(firstElement, finalElement);
        data = new ArrayList<>();
        weights = new ArrayList<ArrayList<Integer>>();
    }

    @Override
    protected void createNewLayer() {
        int pos = data.size();
        ArrayList<Grid> layer = new ArrayList<>();
        if (!(pos == 0)) {
            ArrayList<Integer> weights = this.weights.get(pos - 1);
            if (weights.size() == 0) {
                pos--;
                this.data.remove(pos);
                createNewLayer();
            }
            // int secondBiggestIndex = 0;
            int biggestWeightIndex = 0;
            for (int i = 0; i < this.data.get(pos - 1).size(); i++) {
                if (weights.get(biggestWeightIndex) > weights.get(i) ) {
                    // secondBiggestIndex = biggestWeightIndex;
                    biggestWeightIndex = i;
                }
            }
           /* if (weights.get(biggestWeightIndex) + pos - 1 < this.biggestWeights.get(pos - 2) + pos - 2) {
                pos--;
                this.data.remove(pos);
                createNewLayer();
                return;
            }*/
           System.out.println(biggestWeightIndex);
            this.weights.get(pos - 1).remove(biggestWeightIndex);
                addAllNewCombinations(layer, this.data.get(pos - 1).get(biggestWeightIndex));
            } else {
            addAllNewCombinations(layer, initState);
        }
        this.data.add(layer);
    }

    @Override
    protected void addAllNewCombinations(ArrayList<Grid> layer, Grid initState) {
        Grid newGrid = initState.clone();
        ArrayList<Integer> w = new ArrayList<>();
        newGrid.setParent(initState);
        if (newGrid.moveCell((byte) 0, (byte) 1))
            if (!utils.deepContains(this.data, newGrid)) {
                layer.add(newGrid);
                w.add(createWeights(newGrid));
            }
        newGrid = initState.clone();
        newGrid.setParent(initState);
        if (newGrid.moveCell((byte) 0, (byte) -1))
            if (!utils.deepContains(this.data, newGrid)) {
                layer.add(newGrid);
                w.add(createWeights(newGrid));
            }
        newGrid = initState.clone();
        newGrid.setParent(initState);
        if (newGrid.moveCell((byte) 1, (byte) 0))
            if (!utils.deepContains(this.data, newGrid)) {
                layer.add(newGrid);
                w.add(createWeights(newGrid));
            }
        newGrid = initState.clone();
        newGrid.setParent(initState);
        if (newGrid.moveCell((byte) -1, (byte) 0)) {
            if (!utils.deepContains(this.data, newGrid))
                layer.add(newGrid);
            w.add(createWeights(newGrid));
        }
        System.out.println(w);
        this.weights.add((ArrayList<Integer>) w.clone());
    }

    private int createWeights(Grid newGrid) {
        Cell[][] cells = newGrid.getCells();
        Cell[][] finalCells = finalState.getCells();
        int weight = 0;
        for (int i = 0; i < cells.length; i++) {
            Cell[] cs = cells[i];
            for (int i1 = 0; i1 < cs.length; i1++) {
                Cell cell = cs[i1];
                if (!cell.equals(finalCells[i][i1]))
                    weight++;
            }
        }
        return weight + this.weights.size() - 1;
    }
}
